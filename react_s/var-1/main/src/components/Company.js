import React, { Component } from 'react'

class Company extends Component {
	constructor(props){
		super(props)
		let {item} = this.props
		this.state = {
			name : item.name,
			employees : item.employees,
			revenue : item.revenue,
			isEditing : false
		}
		this.handleChange = (evt) => {
			this.setState({
				[evt.target.name] : evt.target.value
			})
		}
		
		 this.save = () => {
            this.props.onSave(this.props.item.id, {
                name : this.state.name,
                employees : this.state.employees,
                revenue : this.state.revenue
            })
            this.setState({
                isEditing : false
            })
        }
	}
  render() {
    let {item} = this.props
    if (this.state.isEditing){
      return (
        <div>
          <h3>
           <input type="text" name="name" id="name" onChange={this.handleChange} value={this.state.name}/>
          </h3>
          <h3>
           <input type="text" name="employees" id="employees" onChange={this.handleChange} value={this.state.employees}/>
          </h3>
          <h3>
           <input type="text" name="revenue" id="revenue" onChange={this.handleChange} value={this.state.revenue}/>
          </h3>
        
          <input type="button" value="save"  onClick={this.save}/>
          <input type="button" value="cancel" onClick={()=>this.setState({isEditing : false})} />
        </div>
      )
    }
    else{
      return (
        <div>
          Name {item.name} with {item.employees} employees {item.revenue} revenue
          <input type="button" value="edit" onClick={()=>this.setState({isEditing : true})} />        
        </div>
      )
    }
  }
}

export default Company